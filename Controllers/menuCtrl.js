app.controller('menuCtrl', ['$scope', 'personService','roomService',
    function ($scope, personService ,roomService) {
        
        
        
        $scope.status;
        $scope.persons;
        $scope.rooms;
        $scope.showAllPersons = true;
        $scope.showAllRooms = false;     
     
          
             $scope.showPersons = function () {
                 $scope.showAllPersons = !$scope.showAllPersons;
                 $scope.showAllRooms = false;
           
             }
             $scope.showRooms = function () {
                $scope.showAllRooms = !$scope.showAllRooms;
                $scope.showAllPersons = false;
                }
             personService.showPersons()
             .then(function (response) {
                 $scope.persons = response.data;
             }, function (error) {
                 $scope.status = 'Unable to load person data: ' + error.message;
             });

            
                roomService.showRooms()
                .then(function (response) {
                    $scope.rooms = response.data;
                }, function (error) {
                    $scope.status = 'Unable to load room data: ' + error.message;
                });
     
             $scope.deletePerson = function (key) {
                personService.deletePerson(key)
    
                    .then(function (response) {
                        $scope.status = 'Deleted person! Refreshing persons list.';
                        for (var i = 0; i < $scope.persons.length; i++) {
    
                            if ($scope.persons[i].key === key) {
                                $scope.persons.splice(i, 1);
                                break;
                            }
                        }
    
                    }, function (error) {
                        $scope.status = 'Unable to delete person: ' + error.message;
                    });
    
            };     
$scope.deleteRoom = function (key) {
roomService.deleteRoom(key)

    .then(function (response) {
        $scope.status = 'Deleted room! Refreshing rooms list.';
        for (var i = 0; i < $scope.rooms.length; i++) {

            if ($scope.rooms[i].key === key) {
                $scope.rooms.splice(i, 1);
                break;
            }
        }

    }, function (error) {
        $scope.status = 'Unable to delete room: ' + error.message;
    });

};








    


       





        


    }])