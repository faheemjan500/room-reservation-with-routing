app.controller('personCtrl', ['$scope','$routeParams', 'personService',
    function ($scope,$routeParams, personService ) {

        var searchKey = Number($routeParams.personRouteKey);
        $scope.person;
        


        $scope.showPersonInfo = function() {     
            personService.showPerson(searchKey)
            .then(function (response) {
                $scope.person = response.data;
                $scope.firstName = $scope.person.firstName;
                $scope.lastName = $scope.person.lastName;
                $scope.key = $scope.person.key;
            }, function (error) {
                $scope.status = 'Unable to load person data: ' + error.message;
            });
       
           

    }
    $scope.modifyPerson = function(firstName,lastName,key) {
        personService.updatePerson(firstName,lastName,key)
        .then(function (response) {
         $scope.status = 'successfully update person';
        }, function (error) {
            $scope.status = 'Unable to load person data: ' + error.message;
        });

    }

        
    }]);