app.controller('roomCtrl', ['$scope','$routeParams','roomService',
    function ($scope,$routeParams ,roomService) {

       var searchKey = Number($routeParams.roomRouteKey);
        $scope.room;


 $scope.showRoomInfo = function () {
    roomService.showRoom(searchKey)
    .then(function (response) {
        $scope.room = response.data;
        $scope.roomName = $scope.room.roomName;
        $scope.roomSize = $scope.room.roomSize;
        $scope.key = $scope.room.key;
    }, function (error) {
        $scope.status = 'Unable to load room data: ' + error.message;
    });
    
   
    }

}]);