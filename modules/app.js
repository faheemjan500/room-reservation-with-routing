var app = angular.module('app', ["ngRoute" ,"ngMaterial","ngMessages"]);  
  

app.config(['$routeProvider',

     function($routeProvider){
      $routeProvider
      .when("/", {
         templateUrl : "welcome.html",
         controller: "menuCtrl"
       })
      .when('/personInfoRoute/:personRouteKey',{
         templateUrl : "/person.html",
         controller: "personCtrl"
      })
      .when('/roomInfoRoute/:roomRouteKey', {
          templateUrl: "/room.html",
         controller: "roomCtrl"
     })
//      .when('/personChanged', {
//       templateUrl: "/personChanged.html",
//      controller: "personChangedCtrl"
//  })
       .when('/reserveRoom', {
          templateUrl: "/reserveRoom.html",
          controller: "AppCtrl"
       })
  }]);

  